# COWRYWISE SOFTWARE ENGINEER(BACKEND) JOB APPLICATION SOLUTION

## INSTALLATION

1. Clone Repo
2. Change Directory to code directory
3. Confirm you already have pipenv installed by running command "pipenv --version" on terminal
    * If not install pipenv using pip install pipenv or follow instructions at [pipenv](https://pypi.org/project/pipenv/ "Pipenv Documentation")
4. Run command "pipenv shell" in code folder which contains Pipfile
    * Python version for this project is Python 3.9 if you do not have version 3.9 you should edit the Pipfile to your preferred version else download and install Python 3.9 for your system
5. Install code dependecies with command "pipenv install"
6. Create a .env file inside uuid_project folder -> folder that contains the settings.py
7. Following the sample in env.example.* configure your .env file
8. In the code directory -> folder containing manage.py run command "python manage.py migrate"
9. Run test suite with command "pytest -v"
10. Finally run command "python manage.py runserver" this will run the server on http://127.0.0.1:8000

## API DOCUMENTATION

Swagger Docs for the API solution can be found at /dev/documentation

To access the admin portal on command line create superuser with command "python manage.py createsuperuser" follow the prompt. After creating super user login to the admin portal at /admin

