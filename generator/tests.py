from django.http import response
import pytest
from django.urls import reverse
from .utilities import get_key_value_from_response, validate_date_string, validate_uuid_string

solution_url = reverse('solution')
pytestmark = pytest.mark.django_db

# Create your tests here.
def test_solution_should_always_return_elem_with_valid_types(client) -> None:
    '''
        Test will confirm the solution returns success response as well as valid params
        in the response
    '''
    response = client.get(solution_url)
    assert response.status_code == 200

    response = response.json()
    key, value = get_key_value_from_response(response)
    assert validate_date_string(key) == True
    assert validate_uuid_string(value) == True
