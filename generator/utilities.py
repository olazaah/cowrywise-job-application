from datetime import datetime
from uuid import UUID

def get_key_value_from_response(response:dict) -> tuple:
    '''
        returns the first elem in the response dictionary, because we do not know the keys,
        we get the keys and use the first key to get the key:value and return it as (key, value)
    '''
    keys = list(response.keys())
    key = keys[0]
    return (key, response[key])

def validate_date_string(date_string:str) -> bool:
    '''
        returns True or False if string is a date by trying to parse the string into datetime
    '''
    try:
        datetime.strptime(date_string, '%Y-%m-%d %H:%M:%S.%f')
        return True
    except Exception:
        return False

def validate_uuid_string(uuid_string:str) -> bool:
    '''
        returns True or False if a string is valid to uuid version 4
    '''
    try:
        UUID(uuid_string, version=4)
        return True
    except Exception:
        return False