from django.contrib import admin
from .models import UUIDModel

# Register your models here.
@admin.register(UUIDModel)
class UUIDModelAdmin(admin.ModelAdmin):
    list_display =['id', 'date_created']
